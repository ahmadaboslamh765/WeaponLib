function AkimboWeaponBase:_update_bullet_objects(ammo_func)
	AkimboWeaponBase.super._update_bullet_objects(self, ammo_func)

	if alive(self._second_gun) then
		AkimboWeaponBase.super._update_bullet_objects(self._second_gun:base(), ammo_func)
	end
end
