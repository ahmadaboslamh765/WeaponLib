Hooks:PostHook(NewRaycastWeaponBase, "init", "weaponlib_newraycastweaponbase_init", function(self, unit)
	self._bullet_parts = {}
	self._ammo_parts = {}
	self._chamber_parts = {}

	self._parts_steelsight_weapon_visible = {}

	self._scope_index_lookup = {}
	self._scope_part_ids = {}
	self._scope_units = {}
	self._scope_effects = {}
	self._scope_overlays = {}
	self._scope_overlay_border_colors = {}

	-- Fuck of Tdlq, uggghhhh
	if self.fs_reset_methods then
		self.fs_reset_methods = function() end
		self.clip_empty = function()
			return self:ammo_base():get_ammo_remaining_in_clip() == 0
		end
	end
end)

Hooks:PreHook(NewRaycastWeaponBase, "assemble_from_blueprint", "weaponlib_newraycastweaponbase_assemble_from_blueprint", function(self, factory_id, blueprint, clbk)
	local override_weapon_class = nil

	for _, part_id in pairs(blueprint) do
		local part_data = managers.weapon_factory:get_part_data_by_part_id_from_weapon(part_id, factory_id, blueprint)

		if self:is_npc() and part_data.override_npc_weapon_class then
			override_weapon_class = part_data.override_npc_weapon_class
		elseif part_data.override_weapon_class then
			override_weapon_class = part_data.override_weapon_class
		end
	end

	if override_weapon_class then
		local class_table = _G[override_weapon_class]

		if class_table then
			setmetatable(self, __overrides[class_table] or class_table)

			if self.init then
				self.name_id = self._name_id

				self:init(self._unit)

				self.name_id = self._name_id
			end
		end
	end
end)

function NewRaycastWeaponBase:set_gadget_on(gadget_on, ignore_enable, gadgets, current_state)
	if not ignore_enable and not self._enabled then
		return
	end

	if not self._assembly_complete then
		return
	end

	self._gadget_on = gadget_on or self._gadget_on
	local gadget = nil

	for i, id in ipairs(gadgets or self._gadgets) do
		gadget = self._parts[id]

		if gadget and alive(gadget.unit) then
			gadget.unit:base():set_state(self._gadget_on == i, self._sound_fire, current_state)
		end
	end
end

Hooks:PostHook(NewRaycastWeaponBase, "_refresh_gadget_list", "weaponlib_newraycastweaponbase_refresh_gadget_list", function(self)
	local gadgets = self._gadgets
	self._gadgets = {}

	for _, part_id in pairs(gadgets) do
		local part = self._parts[part_id]

		if part and part.unit and part.unit.base and part.unit:base() and part.unit:base().GADGET_TYPE then
			table.insert(self._gadgets, part_id)
		end
	end
end)

Hooks:PostHook(NewRaycastWeaponBase, "clbk_assembly_complete", "weaponlib_newraycastweaponbase_clbk_assembly_complete", function(self, clbk, parts, blueprint)
	self._bullet_parts = {}
	self._ammo_parts = {}
	self._chamber_parts = {}

	self._parts_steelsight_weapon_visible = {}

	self._scope_index_lookup = {}
	self._scope_part_ids = {}
	self._scope_units = {}
	self._scope_effects = {}
	self._scope_overlays = {}
	self._scope_overlay_border_colors = {}

	local second_sight_index = 2

	for part_id, part in pairs(self._parts) do
		local part_data = managers.weapon_factory:get_part_data_by_part_id_from_weapon(part_id, self._factory_id, self._blueprint)

		-- [ self._bullet_parts ]
			local function setup_visual_objects(tweak_data_name, parts_table, max_amount)
				local function objects_for_visual_objects(object_type_prefix)
					local tweak_data_objects_type = object_type_prefix .. tweak_data_name .. "_objects"

					local td = part_data[tweak_data_objects_type]
					if td then
						parts_table[part_id] = parts_table[part_id] or {}
						parts_table[part_id][object_type_prefix .. "objects"] = {
							objects = {},
							step = td.step
						}

						local offset = td.offset or 0
						local prefix = td.prefix

						for object_count = 0 + offset, td.amount + offset do
							local object = part.unit:get_object(Idstring(prefix .. object_count))

							if td.negate then
								object_count = max_amount - object_count
							end

							if object then
								parts_table[part_id][object_type_prefix .. "objects"].objects[object_count] = {object}
							end
						end
					end
				end

				objects_for_visual_objects("")
				objects_for_visual_objects("reverse_")
				objects_for_visual_objects("unique_")

				-- Advanced objects are quite different so handle them differently.
				local td = part_data["advanced_" .. tweak_data_name .. "_objects"]
				if td then
					parts_table[part_id] = parts_table[part_id] or {}
					parts_table[part_id].advanced_objects = {
						objects = {},
						step = td.step
					}

					for index, object_names in pairs(td) do
						number_index = tonumber(index)

						if number_index then
							object_count = math.floor(number_index)

							if td.negate then
								object_count = max_amount - object_count
							end

							parts_table[part_id].advanced_objects.objects[object_count] = {}

							if type(object_names) == "string" then
								object_names = {object_names}
							end

							for _, object_name in pairs(object_names) do
								local object = part.unit:get_object(Idstring(object_name))

								if object then
									table.insert(parts_table[part_id].advanced_objects.objects[object_count], object)
								end
							end
						end
					end
				end
			end

			local chamber_size = self:get_chamber_size() or 0
			local ammo_size = self:get_highest_reload_num()
			local bullet_size = self:ammo_base():get_ammo_max_per_clip() - chamber_size
			if self.AKIMBO then
				bullet_size = bullet_size / 2
				ammo_size = ammo_size / 2
				chamber_size = chamber_size / 2
			end

			local weapon_tweak_data = self:weapon_tweak_data()
			local use_ammo_objects = weapon_tweak_data.use_ammo_objects

			setup_visual_objects("bullet", use_ammo_objects and self._ammo_parts or self._bullet_parts, max_bullets)
			setup_visual_objects("ammo", self._ammo_parts, ammo_size)
			setup_visual_objects("chamber", self._chamber_parts, chamber_size)

		-- [ self._parts_steelsight_weapon_visible ]
			self._parts_steelsight_weapon_visible[part_id] = part_data.steelsight_weapon_visible

		-- [ self._scope_index_lookup, self._scope_part_ids, self._scope_effects, self._scope_overlays, self._scope_overlay_border_colors = {} ]
			local is_sight = part_data.type == "sight"
			if is_sight or part_data.sub_type == "second_sight" then
				local index = 1
				if not is_sight then
					index = second_sight_index
					second_sight_index = second_sight_index + 1
				end

				self._scope_index_lookup[part_id] = index
				self._scope_part_ids[index] = part_id
				self._scope_units[index] = part.unit
				self._scope_effects[index] = part_data.scope_effect or "payday_off"
				self._scope_overlays[index] = part_data.scope_overlay

				-- TODO: Generalise this normalization across all tweak data so you can use this stuff anywhere.
				self._scope_overlay_border_colors[index] = type(part_data.scope_overlay_border_color) == "string" and BeardLib.Utils:normalize_string_value(part_data.scope_overlay_border_color) or part_data.scope_overlay_border_color
			end
	end
end)

function NewRaycastWeaponBase:_update_objects(part_table, amount, custom_object_check)
	-- Might as well check for akimbos here.
	if self.AKIMBO then
		amount = amount / 2

		if self.parent_weapon then
			amount = math.ceil(amount)
		else
			amount = math.floor(amount)
		end
	end

	-- Visibility funcs.
	function default(i, count)
		return i <= count
	end

	function reverse(i, count)
		return i >= count
	end

	function unique(i, count)
		return i == count
	end

	for part_id, object_data in pairs(part_table) do
		local unit = self._parts[part_id].unit

		function check_objects(prefix, visibility_func)
			local data = object_data[prefix .. "objects"]

			if data then
				if custom_object_check then
					custom_object_check(data)
				else
					local step = data.step or 1
					local stepped_amount = math.floor(amount/step)*step

					for object_amount_level, objects in pairs(data.objects) do
						for _, object in pairs(objects) do
							object:set_visibility(visibility_func(object_amount_level, stepped_amount))
						end
					end
				end
			end
		end

		check_objects("", default)
		check_objects("reverse_", reverse)
		check_objects("unique_", unique)
		check_objects("advanced_", unique)
	end
end

function NewRaycastWeaponBase:_update_bullet_objects(ammo_func)
	local chamber_size = self:get_chamber_size()

	local ammo_base = self:ammo_base()
	local ammo = ammo_base[ammo_func](ammo_base)

	-- Cap out the ammo total for weird mags.
	if ammo_func == "get_ammo_total" then
		local total_ammo = ammo_base:get_ammo_total()
		ammo = ammo_base:get_ammo_max_per_clip() - chamber_size

		if total_ammo < ammo then
			ammo = total_ammo
		end
	elseif ammo_func == "get_ammo_remaining_in_clip" then
		ammo = ammo - chamber_size
	end

	ammo = math.max(ammo, 0) -- stop ammo going below zero.

	self:_update_objects(self._bullet_parts, ammo)

	-- Update the chamber.
	self:_update_objects(self._chamber_parts, math.min(ammo_base:get_ammo_remaining_in_clip(), chamber_size))
end

function NewRaycastWeaponBase:weapon_tweak_data(override_id)
	local weapon_id = override_id or self:_weapon_tweak_data_id()
	local factory_id = self._factory_id
	local blueprint = self._blueprint

	if self:_weapon_tweak_data_id() ~= self._name_id or not (factory_id and blueprint) then
		return tweak_data.weapon[weapon_id] or tweak_data.weapon.amcar
	end

	return managers.weapon_factory:get_weapon_tweak_data_override(weapon_id, factory_id, blueprint)
end

Hooks:PostHook(NewRaycastWeaponBase, "_update_stats_values", "weaponlib_newraycastweaponbase_update_stats_values", function(self, disallow_replenish, ammo_data)
	local weapon_tweak_data = self:weapon_tweak_data()

	self._can_shoot_through_shield = weapon_tweak_data.can_shoot_through_shield
	self._can_shoot_through_enemy = weapon_tweak_data.can_shoot_through_enemy
	self._can_shoot_through_wall = weapon_tweak_data.can_shoot_through_wall

	self._rays = weapon_tweak_data.rays or 1
	if self._ammo_data then
		if self._ammo_data and self._ammo_data.rays ~= nil then
			self._rays = self._ammo_data.rays
		end

		if self._ammo_data.can_shoot_through_shield ~= nil then
			self._can_shoot_through_shield = self._ammo_data.can_shoot_through_shield
		end

		if self._ammo_data.can_shoot_through_enemy ~= nil then
			self._can_shoot_through_enemy = self._ammo_data.can_shoot_through_enemy
		end

		if self._ammo_data.can_shoot_through_wall ~= nil then
			self._can_shoot_through_wall = self._ammo_data.can_shoot_through_wall
		end
	end
end)

local mvec_to = Vector3()
local mvec_direction = Vector3()
local mvec_spread_direction = Vector3()

function NewRaycastWeaponBase:_fire_raycast(user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul)
	if self:gadget_overrides_weapon_functions() then
		return self:gadget_function_override("_fire_raycast", self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul)
	end

	local result = {}

	local damage = self:_get_current_damage(dmg_mul)
	local hit_anyone = false
	local auto_hit_candidate, suppression_enemies = self:check_autoaim(from_pos, direction)
	
	local spread_x, spread_y = self:_get_spread(user_unit)
	local ray_distance = self:weapon_range()
	local right = direction:cross(Vector3(0, 0, 1)):normalized()
	local up = direction:cross(right):normalized()

	local hit_rays = {}
	local overall_ray_hits = {}

	local already_hit_enemies = {}

	for i = 1, self._rays or 1 do
		local theta = math.random() * 360
		local ax = math.sin(theta) * math.random() * spread_x * (spread_mul or 1)
		local ay = math.cos(theta) * math.random() * spread_y * (spread_mul or 1)

		mvector3.set(mvec_spread_direction, direction)
		mvector3.add(mvec_spread_direction, right * math.rad(ax))
		mvector3.add(mvec_spread_direction, up * math.rad(ay))
		mvector3.set(mvec_to, mvec_spread_direction)
		mvector3.multiply(mvec_to, ray_distance)
		mvector3.add(mvec_to, from_pos)

		local ray_hits, hit_enemy = self:_collect_hits(from_pos, mvec_to)

		if suppression_enemies and self._suppression then
			result.enemies_in_cone = suppression_enemies
		end

		if self._autoaim then
			local weight = 0.1

			if auto_hit_candidate and not hit_enemy then
				local autohit_chance = 1 - math.clamp((self._autohit_current - self._autohit_data.MIN_RATIO) / (self._autohit_data.MAX_RATIO - self._autohit_data.MIN_RATIO), 0, 1)

				if autohit_mul then
					autohit_chance = autohit_chance * autohit_mul
				end

				if math.random() < autohit_chance then
					self._autohit_current = (self._autohit_current + weight) / (1 + weight)

					mvector3.set(mvec_to, from_pos)
					mvector3.add_scaled(mvec_to, auto_hit_candidate.ray, ray_distance)

					ray_hits, hit_enemy = self:_collect_hits(from_pos, mvec_to)
				end
			end

			if hit_enemy then
				self._autohit_current = (self._autohit_current + weight) / (1 + weight)
			elseif auto_hit_candidate then
				self._autohit_current = self._autohit_current / (1 + weight)
			end
		end

		local enemies_hit_in_this_pass = {}
		for index, hit in ipairs(ray_hits) do
			local visual = false

			if hit.unit:character_damage() then
				if already_hit_enemies[hit.unit:key()] then
					visual = true
				else
					table.insert(enemies_hit_in_this_pass, hit.unit:key())
					table.insert(hit_rays, hit)
				end
			end

			table.insert(overall_ray_hits, {
				hit = hit,
				visual = visual,
				layer = index
			})
		end

		for _, enemy_key in ipairs(enemies_hit_in_this_pass) do
			already_hit_enemies[enemy_key] = true
		end
	end

	table.sort(overall_ray_hits, function(hit_info_a, hit_info_b)
		return hit_info_a.layer < hit_info_b.layer
	end)

	local hit_count = 0
	local cop_kill_count = 0
	local hit_through_wall = false
	local hit_through_shield = false
	local hit_result = nil

	local shotgun_kill_data = {
		kills = 0,
		headshots = 0,
		civilian_kills = 0
	}

	local hit_results = {}
	for index, hit_info in ipairs(overall_ray_hits) do
		local hit = hit_info.hit

		damage = self:get_damage_falloff(damage, hit, user_unit)
		if damage > 0 then
			if hit_info.visual then
				self._bullet_class:on_collision_effects(hit, self._unit, user_unit, damage)
			else
				local hit_result = self._bullet_class:on_collision(hit, self._unit, user_unit, damage)

				if hit_result then
					table.insert(hit_results, {
						hit_result,
						hit
					})
				end
			end
		end
	end

	managers.statistics:shot_fired({
		hit = false,
		weapon_unit = self._unit
	})

	local is_shotgun = self:is_category("shotgun")
	for _, hit_result_info in ipairs(hit_results) do
		local hit_result = hit_result_info[1]
		local hit = hit_result_info[2]

		if is_shotgun then
			hit_result = managers.mutators:modify_value("ShotgunBase:_fire_raycast", hit_result)

			if hit_result and hit_result.type == "death" then
				shotgun_kill_data.kills = shotgun_kill_data.kills + 1

				if hit.body and hit.body:name() == Idstring("head") then
					shotgun_kill_data.headshots = shotgun_kill_data.headshots + 1
				end

				if hit.unit and hit.unit:base() and (hit.unit:base()._tweak_table == "civilian" or hit.unit:base()._tweak_table == "civilian_female") then
					shotgun_kill_data.civilian_kills = shotgun_kill_data.civilian_kills + 1
				end
			end
		end

		if hit_result.type == "death" then
			local unit_type = hit.unit:base() and hit.unit:base()._tweak_table
			local is_civilian = unit_type and CopDamage.is_civilian(unit_type)

			if not is_civilian then
				cop_kill_count = cop_kill_count + 1
			end

			if self:is_category(tweak_data.achievement.easy_as_breathing.weapon_type) and not is_civilian then
				self._kills_without_releasing_trigger = (self._kills_without_releasing_trigger or 0) + 1

				if tweak_data.achievement.easy_as_breathing.count <= self._kills_without_releasing_trigger then
					managers.achievment:award(tweak_data.achievement.easy_as_breathing.award)
				end
			end
		end

		hit.damage_result = hit_result
		hit_anyone = true
		hit_count = hit_count + 1

		if hit.unit:in_slot(managers.slot:get_mask("world_geometry")) then
			hit_through_wall = true
		elseif hit.unit:in_slot(managers.slot:get_mask("enemy_shield_check")) then
			hit_through_shield = hit_through_shield or alive(hit.unit:parent())
		end

		if hit_result.type == "death" and cop_kill_count > 0 then
			local unit_type = hit.unit:base() and hit.unit:base()._tweak_table
			local multi_kill, enemy_pass, obstacle_pass, weapon_pass, weapons_pass, weapon_type_pass = nil

			for achievement, achievement_data in pairs(tweak_data.achievement.sniper_kill_achievements) do
				multi_kill = not achievement_data.multi_kill or cop_kill_count == achievement_data.multi_kill
				enemy_pass = not achievement_data.enemy or unit_type == achievement_data.enemy
				obstacle_pass = not achievement_data.obstacle or achievement_data.obstacle == "wall" and hit_through_wall or achievement_data.obstacle == "shield" and hit_through_shield
				weapon_pass = not achievement_data.weapon or self._name_id == achievement_data.weapon
				weapons_pass = not achievement_data.weapons or table.contains(achievement_data.weapons, self._name_id)
				weapon_type_pass = not achievement_data.weapon_type or self:is_category(achievement_data.weapon_type)

				if multi_kill and enemy_pass and obstacle_pass and weapon_pass and weapons_pass and weapon_type_pass then
					if achievement_data.stat then
						managers.achievment:award_progress(achievement_data.stat)
					elseif achievement_data.award then
						managers.achievment:award(achievement_data.award)
					elseif achievement_data.challenge_stat then
						managers.challenge:award_progress(achievement_data.challenge_stat)
					elseif achievement_data.trophy_stat then
						managers.custom_safehouse:award(achievement_data.trophy_stat)
					elseif achievement_data.challenge_award then
						managers.challenge:award(achievement_data.challenge_award)
					end
				end
			end
		end

		if (not self._ammo_data or not self._ammo_data.ignore_statistic) then
			managers.statistics:shot_fired({
				skip_bullet_count = true,
				hit = true,
				weapon_unit = self._unit
			})
		end
	end

	if not tweak_data.achievement.tango_4.difficulty or table.contains(tweak_data.achievement.tango_4.difficulty, Global.game_settings.difficulty) then
		if self._gadgets and table.contains(self._gadgets, "wpn_fps_upg_o_45rds") and cop_kill_count > 0 and managers.player:player_unit():movement():current_state():in_steelsight() then
			if self._tango_4_data then
				if self._gadget_on == self._tango_4_data.last_gadget_state then
					self._tango_4_data = nil
				else
					self._tango_4_data.last_gadget_state = self._gadget_on
					self._tango_4_data.count = self._tango_4_data.count + 1
				end

				if self._tango_4_data and tweak_data.achievement.tango_4.count <= self._tango_4_data.count then
					managers.achievment:_award_achievement(tweak_data.achievement.tango_4, "tango_4")
				end
			else
				self._tango_4_data = {
					count = 1,
					last_gadget_state = self._gadget_on
				}
			end
		elseif self._tango_4_data then
			self._tango_4_data = nil
		end
	end

	result.hit_enemy = hit_anyone

	if self._autoaim then
		self._shot_fired_stats_table.hit = hit_anyone
		self._shot_fired_stats_table.hit_count = hit_count
	end

	local furthest_hit = hit_rays[#hit_rays]
	if (furthest_hit and furthest_hit.distance > 600 or not furthest_hit) and alive(self._obj_fire) then
		self._obj_fire:m_position(self._trail_effect_table.position)
		mvector3.set(self._trail_effect_table.normal, mvec_spread_direction)

		local trail = World:effect_manager():spawn(self._trail_effect_table)

		if furthest_hit then
			World:effect_manager():set_remaining_lifetime(trail, math.clamp((furthest_hit.distance - 600) / 10000, 0, furthest_hit.distance))
		end
	end

	if self._alert_events then
		result.rays = hit_rays
	end

	if is_shotgun then
		for key, data in pairs(tweak_data.achievement.shotgun_single_shot_kills) do
			if data.headshot and data.count <= shotgun_kill_data.headshots - shotgun_kill_data.civilian_kills or data.count <= shotgun_kill_data.kills - shotgun_kill_data.civilian_kills then
				local should_award = true

				if data.blueprint then
					local missing_parts = false

					for _, part_or_parts in ipairs(data.blueprint) do
						if type(part_or_parts) == "string" then
							if not table.contains(self._blueprint or {}, part_or_parts) then
								missing_parts = true

								break
							end
						else
							local found_part = false

							for _, part in ipairs(part_or_parts) do
								if table.contains(self._blueprint or {}, part) then
									found_part = true

									break
								end
							end

							if not found_part then
								missing_parts = true

								break
							end
						end
					end

					if missing_parts then
						should_award = false
					end
				end

				if should_award then
					managers.achievment:_award_achievement(data, key)
				end
			end
		end
	end

	return result
end

Hooks:PostHook(NewRaycastWeaponBase, "replenish", "weaponlib_newraycastweaponbase_replenish", function(self)
	local original_tweak_data = tweak_data.weapon[self._name_id]
	local weapon_tweak_data = self:weapon_tweak_data()

	local ammo_max_multiplier = managers.player:upgrade_value("player", "extra_ammo_multiplier", 1)

	for _, category in ipairs(weapon_tweak_data.categories) do
		ammo_max_multiplier = ammo_max_multiplier * managers.player:upgrade_value(category, "extra_ammo_multiplier", 1)
	end

	ammo_max_multiplier = ammo_max_multiplier + ammo_max_multiplier * (self._total_ammo_mod or 0)

	if managers.player:has_category_upgrade("player", "add_armor_stat_skill_ammo_mul") then
		ammo_max_multiplier = ammo_max_multiplier * managers.player:body_armor_value("skill_ammo_mul", nil, 1)
	end

	ammo_max_multiplier = managers.modifiers:modify_value("WeaponBase:GetMaxAmmoMultiplier", ammo_max_multiplier)
	local ammo_max_per_clip = self:calculate_ammo_max_per_clip()

	local ammo_max_override_delta = weapon_tweak_data.AMMO_MAX - original_tweak_data.AMMO_MAX
	local ammo_max = math.round(((original_tweak_data.AMMO_MAX + (managers.player:upgrade_value(self._name_id, "clip_amount_increase") * ammo_max_per_clip)) * ammo_max_multiplier) + ammo_max_override_delta)
	ammo_max_per_clip = math.min(ammo_max_per_clip, ammo_max)

	self:set_ammo_max_per_clip(ammo_max_per_clip + self:get_chamber_size())
	self:set_ammo_max(ammo_max)
	self:set_ammo_total(ammo_max)
	self:set_ammo_remaining_in_clip(ammo_max_per_clip)

	self._ammo_pickup = weapon_tweak_data.AMMO_PICKUP
end)

function NewRaycastWeaponBase:calculate_ammo_max_per_clip()
	local added = 0
	local weapon_tweak_data = self:weapon_tweak_data()

	if self:is_category("shotgun") and weapon_tweak_data.has_magazine then
		added = managers.player:upgrade_value("shotgun", "magazine_capacity_inc", 0)

		if self:is_category("akimbo") then
			added = added * 2
		end
	elseif self:is_category("pistol") and not self:is_category("revolver") and managers.player:has_category_upgrade("pistol", "magazine_capacity_inc") then
		added = managers.player:upgrade_value("pistol", "magazine_capacity_inc", 0)

		if self:is_category("akimbo") then
			added = added * 2
		end
	elseif self:is_category("smg", "assault_rifle", "lmg") then
		added = managers.player:upgrade_value("player", "automatic_mag_increase", 0)

		if self:is_category("akimbo") then
			added = added * 2
		end
	end

	local ammo = weapon_tweak_data.CLIP_AMMO_MAX + added
	ammo = ammo + managers.player:upgrade_value(self._name_id, "clip_ammo_increase")

	if not self:upgrade_blocked("weapon", "clip_ammo_increase") then
		ammo = ammo + managers.player:upgrade_value("weapon", "clip_ammo_increase", 0)
	end

	for _, category in ipairs(weapon_tweak_data.categories) do
		if not self:upgrade_blocked(category, "clip_ammo_increase") then
			ammo = ammo + managers.player:upgrade_value(category, "clip_ammo_increase", 0)
		end
	end

	ammo = ammo + (self._extra_ammo or 0)

	return ammo
end

function NewRaycastWeaponBase:recoil_wait()
	local tweak_is_auto = self:weapon_tweak_data().FIRE_MODE == "auto"
	local weapon_is_auto = self:fire_mode() == "auto"

	if not tweak_is_auto then
		return nil
	end

	local multiplier = tweak_is_auto == weapon_is_auto and 1 or 2

	return self:weapon_tweak_data().fire_mode_data.fire_rate * multiplier
end

function RaycastWeaponBase:update_next_shooting_time()
	if self:gadget_overrides_weapon_functions() then
		local gadget_func = self:gadget_function_override("update_next_shooting_time")

		if gadget_func then
			return gadget_func
		end
	end

	local tweak_data = self:weapon_tweak_data()
	local next_fire = (tweak_data.fire_mode_data and tweak_data.fire_mode_data.fire_rate or 0) / self:fire_rate_multiplier()
	self._next_fire_allowed = self._next_fire_allowed + next_fire
end

function NewRaycastWeaponBase:get_name_id()
	if self:gadget_overrides_weapon_functions() then
		return self:gadget_function_override("get_name_id")
	end

	return self._name_id
end

function NewRaycastWeaponBase:stop_shooting()
	if self:gadget_overrides_weapon_functions() then
		local gadget_func = self:gadget_function_override("stop_shooting")

		if gadget_func then
			return gadget_func
		end
	end

	RaycastWeaponBase.stop_shooting(self)
end

function RaycastWeaponBase:get_stance_id()
	return self:weapon_tweak_data().use_stance or self:weapon_tweak_data(self._name_id).use_stance or self._name_id
end

function NewRaycastWeaponBase:weapon_hold()
	return self:weapon_tweak_data().weapon_hold or self:weapon_tweak_data(self._name_id).weapon_hold or self._name_id
end

function NewRaycastWeaponBase:reload_name_id()
	local initial_td = self:weapon_tweak_data()
	if initial_td.animations and initial_td.animations.reload_name_id then
		return initial_td.animations.reload_name_id
	end

	local second_td = self:weapon_tweak_data(self._name_id)
	if second_td.animations and second_td.animations.reload_name_id then
		return second_td.animations.reload_name_id
	end

	return self._name_id
end

-- Reloading
	function RaycastWeaponBase:can_reload()
		local ammo_remaining_in_clip = self:ammo_base():get_ammo_remaining_in_clip()
		return ammo_remaining_in_clip < self:ammo_base():get_ammo_total() and ammo_remaining_in_clip <= self:get_reload_threshold(true)
	end

	function NewRaycastWeaponBase:should_do_empty_reload()
		return self:ammo_base():get_ammo_remaining_in_clip() <= self:get_reload_threshold(false)
	end

	function NewRaycastWeaponBase:get_reload_threshold(is_not_empty)
		if is_not_empty then
			return self:weapon_tweak_data().reload_threshold or (self:ammo_base():get_ammo_max_per_clip() - 1)
		else
			return self:weapon_tweak_data().empty_reload_threshold or 0
		end
	end

	function NewRaycastWeaponBase:get_chamber_size()
		return self:weapon_tweak_data().chamber_size or 0
	end

	function NewRaycastWeaponBase:get_reload_nums(is_not_empty)
		local weapon_tweak_data = self:weapon_tweak_data()

		local default = self:use_shotgun_reload() and 1 or (self:ammo_base():get_ammo_max_per_clip() - self:get_chamber_size() or 0)

		local reload_nums = { weapon_tweak_data.reload_num or default }
		if not is_not_empty then
			reload_nums = { (weapon_tweak_data.empty_reload_num or weapon_tweak_data.reload_num) or default }
		end

		local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)
		if shotgun_reload_tweak then
			if shotgun_reload_tweak.reload_queue then
				for index, queue_data in pairs(shotgun_reload_tweak.reload_queue) do
					reload_nums[index] = queue_data.reload_num
				end
			elseif shotgun_reload_tweak.reload_num then
				reload_nums = { shotgun_reload_tweak.reload_num }
			end
		end

		return reload_nums
	end

	function NewRaycastWeaponBase:reload_expire_t(is_not_empty)
		if self:use_shotgun_reload() then
			local ammo_total = self:ammo_base():get_ammo_total()
			local ammo_max_per_clip = self:ammo_base():get_ammo_max_per_clip()
			local ammo_remaining_in_clip = self:ammo_base():get_ammo_remaining_in_clip()
			local ammo_to_reload = math.min(ammo_total - ammo_remaining_in_clip, ammo_max_per_clip - ammo_remaining_in_clip)
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)

			if shotgun_reload_tweak and shotgun_reload_tweak.reload_queue then
				local reload_num_table = self:get_reload_nums(is_not_empty)

				local reload_expire_t = 0
				local queue_index = 0
				local queue_data = nil
				local queue_num = #shotgun_reload_tweak.reload_queue

				while ammo_to_reload > 0 do
					if queue_index == queue_num then
						reload_expire_t = reload_expire_t + (shotgun_reload_tweak.reload_queue_wrap or 0)
					end

					queue_index = queue_index % queue_num + 1
					queue_data = shotgun_reload_tweak.reload_queue[queue_index]
					reload_expire_t = reload_expire_t + queue_data.expire_t or 0.5666666666666667

					ammo_to_reload = ammo_to_reload - (reload_num_table and reload_num_table[queue_index] or 1)
				end

				return reload_expire_t
			end

			local reload_shell_expire_t = self:reload_shell_expire_t(is_not_empty)
			local reload_num = self:get_clamped_reload_num()

			return math.ceil(ammo_to_reload / reload_num) * reload_shell_expire_t
		end

		return nil
	end

	function NewRaycastWeaponBase:reload_enter_expire_t(is_not_empty)
		if self:use_shotgun_reload() then
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)

			return shotgun_reload_tweak and shotgun_reload_tweak.reload_enter or self:weapon_tweak_data().timers.shotgun_reload_enter or 0.3
		end

		return nil
	end

	function NewRaycastWeaponBase:reload_exit_expire_t(is_not_empty)
		if self:use_shotgun_reload() then
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)

			if shotgun_reload_tweak then
				return shotgun_reload_tweak.reload_exit
			end

			if is_not_empty then
				return self:weapon_tweak_data().timers.shotgun_reload_exit_not_empty or 0.3
			end

			return self:weapon_tweak_data().timers.shotgun_reload_exit_empty or 0.7
		end

		return nil
	end

	function NewRaycastWeaponBase:reload_shell_expire_t(is_not_empty)
		if self:use_shotgun_reload() then
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)

			return shotgun_reload_tweak and shotgun_reload_tweak.reload_shell or self:weapon_tweak_data().timers.shotgun_reload_shell or 0.5666666666666667
		end

		return nil
	end

	function NewRaycastWeaponBase:_first_shell_reload_expire_t(is_not_empty)
		if self:use_shotgun_reload() then
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(is_not_empty)
			local first_shell_offset = shotgun_reload_tweak and shotgun_reload_tweak.reload_first_shell_offset or self:weapon_tweak_data().timers.shotgun_reload_first_shell_offset or 0.33

			return self:reload_shell_expire_t(is_not_empty) - first_shell_offset
		end

		return nil
	end

	-- Thanks overkill, :c
	function NewRaycastWeaponBase:update_ammo_objects()
		local clamped_reload_num = self:get_clamped_reload_num()

		local custom_object_check = nil
		local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(not self:started_reload_empty())
		if shotgun_reload_tweak and shotgun_reload_tweak.reload_queue then
			custom_object_check = function(data)
				local ammo_remaining_in_clip = self:ammo_base():get_ammo_remaining_in_clip()
				local ammo_available = math.min(self:ammo_base():get_ammo_total() - ammo_remaining_in_clip, self:ammo_base():get_ammo_max_per_clip() - ammo_remaining_in_clip)

				local queue_num = #shotgun_reload_tweak.reload_queue
				local queue_index = self._shotgun_queue_index % queue_num + 1
				local queue_data = shotgun_reload_tweak.reload_queue[queue_index]
				local queue_shell_order = queue_data and queue_data.shell_order
				local ammo_to_reload = 0

				repeat
					ammo_to_reload = ammo_to_reload + math.min(ammo_available, clamped_reload_num)
					ammo_available = ammo_available - clamped_reload_num
				until ammo_available <= 0 or queue_data and queue_data.stop_update_ammo

				local object_i = nil

				for object_amount_level, objects in pairs(data.objects) do
					for _, object in pairs(objects) do
						if queue_shell_order then
							object_i = table.get_vector_index(queue_shell_order, object_amount_level)
						else
							object_i = object_amount_level
						end

						object:set_visibility(object_i and object_i <= ammo_to_reload)
					end
				end
			end
		end

		self:_update_objects(self._ammo_parts, clamped_reload_num, custom_object_check)
	end

	function NewRaycastWeaponBase:start_reload(...)
		NewRaycastWeaponBase.super.start_reload(self, ...)

		self._started_reload_empty = self:should_do_empty_reload()

		if self:use_shotgun_reload() then
			local speed_multiplier = self:reload_speed_multiplier()
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(not self:started_reload_empty())
			local t = managers.player:player_timer():time()

			self._shotgun_queue_index = nil
			if shotgun_reload_tweak and shotgun_reload_tweak.reload_queue then
				self._shotgun_queue_index = 0
				local next_queue_data = shotgun_reload_tweak.reload_queue[1]
				self._next_shell_reloded_t = t + next_queue_data.expire_t / speed_multiplier

				if not next_queue_data.skip_update_ammo then
					self:update_ammo_objects()
				end
			else
				self._next_shell_reloded_t = t + self:_first_shell_reload_expire_t(not self._started_reload_empty) / speed_multiplier

				self:update_ammo_objects()
			end

			self._current_reload_speed_multiplier = speed_multiplier
		else
			self:update_ammo_objects()
		end
	end

	function NewRaycastWeaponBase:started_reload_empty()
		return self._started_reload_empty
	end

	function NewRaycastWeaponBase:get_highest_reload_num()
		local highest = 0

		for _, num in pairs(self:get_reload_nums(false)) do
			if num > highest then highest = num end
		end

		for _, num in pairs(self:get_reload_nums(true)) do
			if num > highest then highest = num end
		end

		return highest
	end

	function NewRaycastWeaponBase:get_reload_num()
		-- Beardlib doesn't check all it's shit properly so I can't use `self:started_reload_empty()` here.
		local reload_num_table = self:get_reload_nums(not self._started_reload_empty)

		local default = self:use_shotgun_reload() and 1 or (self:ammo_base():get_ammo_max_per_clip() - self:get_chamber_size() or 0)
		local reload_num = reload_num_table and reload_num_table[self._shotgun_queue_index or 1] or default

		return reload_num
	end

	function NewRaycastWeaponBase:get_clamped_reload_num()
		local reload_num = self:get_reload_num()

		if self._setup.expend_ammo then
			local ammo_remaining_in_clip = self:ammo_base():get_ammo_remaining_in_clip()

			local space_left = self:ammo_base():get_ammo_max_per_clip() - ammo_remaining_in_clip
			local ammo_left = self:ammo_base():get_ammo_total() - ammo_remaining_in_clip

			return math.min(math.min(space_left, reload_num), ammo_left)
		else
			return reload_num
		end
	end

	function NewRaycastWeaponBase:update_reloading(t, dt, time_left)
		if self:use_shotgun_reload() and self._next_shell_reloded_t and self._next_shell_reloded_t < t then
			local speed_multiplier = self:reload_speed_multiplier()
			local shotgun_reload_tweak = self:_get_shotgun_reload_tweak_data(not self:started_reload_empty())
			local next_queue_data = nil

			if shotgun_reload_tweak and shotgun_reload_tweak.reload_queue then
				self._shotgun_queue_index = self._shotgun_queue_index % #shotgun_reload_tweak.reload_queue + 1

				if self._shotgun_queue_index == #shotgun_reload_tweak.reload_queue then
					self._next_shell_reloded_t = self._next_shell_reloded_t + (shotgun_reload_tweak.reload_queue_wrap or 0)
				end

				next_queue_data = shotgun_reload_tweak.reload_queue[self._shotgun_queue_index + 1]
				self._next_shell_reloded_t = self._next_shell_reloded_t + (next_queue_data and next_queue_data.expire_t or 0.5666666666666667) / speed_multiplier
			else
				self._next_shell_reloded_t = self._next_shell_reloded_t + self:reload_shell_expire_t(not self._started_reload_empty) / speed_multiplier
			end

			local ammo_to_reload = self:get_clamped_reload_num()

			self:ammo_base():set_ammo_remaining_in_clip(math.min(self:ammo_base():get_ammo_total(), self:ammo_base():get_ammo_max_per_clip(), self:ammo_base():get_ammo_remaining_in_clip() + ammo_to_reload))
			managers.job:set_memory("kill_count_no_reload_" .. tostring(self._name_id), nil, true)

			if not next_queue_data or not next_queue_data.skip_update_ammo then
				self:update_ammo_objects()
			end

			return true
		end
	end

	function NewRaycastWeaponBase:on_reload(amount)
		self._shotgun_queue_index = nil

		amount = amount or (self:ammo_base():get_ammo_remaining_in_clip() + self:get_clamped_reload_num())

		if self._setup.expend_ammo then
			self:ammo_base():set_ammo_remaining_in_clip(math.min(self:ammo_base():get_ammo_total(), amount))
		else
			self:ammo_base():set_ammo_remaining_in_clip(amount)
			self:ammo_base():set_ammo_total(amount)
		end

		managers.job:set_memory("kill_count_no_reload_" .. tostring(self._name_id), nil, true)

		local user_unit = managers.player:player_unit()
		if user_unit then
			user_unit:movement():current_state():send_reload_interupt()
		end

		self:set_reload_objects_visible(false)

		self._reload_objects = {}
	end

	function NewRaycastWeaponBase:reload_interuptable()
		if self:use_shotgun_reload() then
			return true
		end

		return false
	end

	function NewRaycastWeaponBase:shotgun_shell_data()
		if self:use_shotgun_reload() then
			local reload_shell_data = self:weapon_tweak_data().animations.reload_shell_data
			local unit_name = reload_shell_data and reload_shell_data.unit_name or "units/payday2/weapons/wpn_fps_shell/wpn_fps_shell"
			local align = reload_shell_data and reload_shell_data.align or nil

			return {
				unit_name = unit_name,
				align = align
			}
		end

		return nil
	end

	function NewRaycastWeaponBase:use_shotgun_reload()
		if self:gadget_overrides_weapon_functions() then
			return self:gadget_function_override("use_shotgun_reload")
		end

		return self._use_shotgun_reload
	end

-- Scopes
	function NewRaycastWeaponBase:zoom()
		local scope_index = self:get_active_scope_index()

		if self._scope_part_ids and scope_index > 1 then
			local part_id = self._scope_part_ids[scope_index]
			local part_data = managers.weapon_factory:get_part_data_by_part_id_from_weapon(part_id, self._factory_id, self._blueprint)

			local gadget_zoom_stat = part_data.stats and part_data.stats.gadget_zoom or NewRaycastWeaponBase.super.zoom(self)
			local gadget_zoom_add_stat = part_data.stats and part_data.stats.gadget_zoom_add or 0

			local zoom_index = math.min(gadget_zoom_stat + gadget_zoom_add_stat, #tweak_data.weapon.stats.zoom)

			return tweak_data.weapon.stats.zoom[zoom_index]
		end

		return NewRaycastWeaponBase.super.zoom(self)
	end

	function NewRaycastWeaponBase:is_second_sight_on()
		return self:get_active_scope_index() > 1
	end

	function NewRaycastWeaponBase:stance_mod()
		if not self._blueprint or not self._factory_id then
			return nil
		end

		local scope_index = self:get_active_scope_index()
		local scope_part_id = self._scope_part_ids and self._scope_part_ids[scope_index]

		return managers.weapon_factory:get_stance_mod_scope_part_id(self._factory_id, self._blueprint, scope_part_id)
	end

	function NewRaycastWeaponBase:get_active_scope_index()
		if self._scope_units then
			for index, scope_unit in pairs(self._scope_units) do
				local scope_unit_base = scope_unit:base()

				if scope_unit_base and scope_unit_base.is_on and scope_unit_base:is_on() then
					return index
				end
			end
		end

		return 1
	end

	function NewRaycastWeaponBase:set_visual_scope_index(scope_index)
		self._visual_scope_index = scope_index
	end

	function NewRaycastWeaponBase:_set_parts_visible(visible)
		if self._parts then
			local hide_weapon = false

			for part_id, data in pairs(self._parts) do
				local parent = data.parent and managers.weapon_factory:get_part_id_from_weapon_by_type(data.parent, self._blueprint)
				local scope_index = self._scope_index_lookup and (self._scope_index_lookup[part_id] or (parent and self._scope_index_lookup[parent])) or 1

				local unit = data.unit or data.link_to_unit

				local steelsight_swap_state = scope_index == self._visual_scope_index

				if steelsight_swap_state and (self._parts_steelsight_weapon_visible and self._parts_steelsight_weapon_visible[part_id] == false) then
					hide_weapon = true
					break
				end

				if alive(unit) then
					local is_visible = visible and (self._parts[part_id].steelsight_visible == nil or self._parts[part_id].steelsight_visible == steelsight_swap_state)

					unit:set_visible(is_visible)
				end
			end

			if hide_weapon then
				for part_id, data in pairs(self._parts) do
					local unit = data.unit or data.link_to_unit

					if alive(unit) then
						unit:set_visible(false)
					end
				end

				self._unit:set_visible(false)
			else
				self._unit:set_visible(true)
			end
		end

		self:_chk_charm_upd_state()
	end

	function NewRaycastWeaponBase:get_scope_effect(scope_index)
		return self._scope_effects and self._scope_effects[scope_index] or "payday_off"
	end

	function NewRaycastWeaponBase:get_scope_overlay(scope_index)
		return self._scope_overlays and self._scope_overlays[scope_index] or nil
	end

	function NewRaycastWeaponBase:get_scope_overlay_border_color(scope_index)
		return self._scope_overlay_border_colors and self._scope_overlay_border_colors[scope_index] or Color.black
	end

-- Override Sounds
	function NewRaycastWeaponBase:_get_sound_event(event, alternative_event)
		local sounds = self:weapon_tweak_data().sounds
		local event = sounds and (sounds[event] or sounds[alternative_event])

		return event
	end

-- Underbarrel Toggle
	function NewRaycastWeaponBase:setup_underbarrel_data()
		local underbarrel_prefix = "underbarrel_"

		for part_id, part in pairs(self._parts) do
			local part_unit = part.unit

			if part_unit and alive(part_unit) and part_unit.base and part_unit:base() and part_unit:base().GADGET_TYPE and part_unit:base().toggle then
				if string.sub(part_unit:base().GADGET_TYPE, 1, #underbarrel_prefix) == underbarrel_prefix then
					self._underbarrel_part = part
					break
				end
			end
		end

		local underbarrel_ammo_data = managers.weapon_factory:get_part_data_type_from_weapon_by_type("underbarrel_ammo", "custom_stats", self._parts)
		if self._underbarrel_part then
			self._underbarrel_part.unit:base():setup_data(self._setup, 1, underbarrel_ammo_data)
		end
	end

	function NewRaycastWeaponBase:underbarrel_toggle()
		if self._underbarrel_part then
			self._underbarrel_part.unit:base():toggle()
			return self._underbarrel_part.unit:base():is_on()
		end

		return nil
	end

	function NewRaycastWeaponBase:underbarrel_name_id()
		if self._underbarrel_part then
			return self._underbarrel_part.unit:base().name_id or self._underbarrel_part.unit:base()._name_id
		end
	end

-- New Burst Fire Stuff
	NewRaycastWeaponBase.firemodes = {
		{
			"single",
			Idstring("single"),
			true,
			"wp_auto_switch_off"
		},
		{
			"auto",
			Idstring("auto"),
			true,
			"wp_auto_switch_on"
		},
		{
			"burst",
			Idstring("burst"),
			false,
			"wp_auto_switch_on"
		}
	}

	NewRaycastWeaponBase.firemode_index_lookup = {}
	for index, firemode in pairs(NewRaycastWeaponBase.firemodes) do
		NewRaycastWeaponBase.firemode_index_lookup[firemode[2]:key()] = index
	end

	function NewRaycastWeaponBase:_can_use_firemode_index(firemode_index)
		local firemode_data = self.firemodes[firemode_index]
		local default = firemode_data[3]

		local toggle_firemode_table = self:weapon_tweak_data().CAN_TOGGLE_SPECIFIC_FIREMODE
		if not toggle_firemode_table then return default end

		local firemode_string = firemode_data[1]
		local can_do = toggle_firemode_table[firemode_string]
		if can_do == nil then return default end

		return not not can_do
	end

	function NewRaycastWeaponBase:toggle_firemode(skip_post_event)
		local can_toggle = not self._locked_fire_mode and self:can_toggle_firemode()

		if can_toggle then
			local current_firemode_index = self.firemode_index_lookup[self._fire_mode:key()]
			local next_firemode_index = current_firemode_index

			repeat
			    next_firemode_index = (next_firemode_index % #self.firemodes) + 1
			until self:_can_use_firemode_index(next_firemode_index) or (next_firemode_index == current_firemode_index) -- If we loop all the way around safety check to not infinite loop.

			local next_firemode_data = self.firemodes[next_firemode_index]

			self._fire_mode = next_firemode_data[2]
			if not skip_post_event and next_firemode_data[4] then
			    self._sound_fire:post_event(next_firemode_data[4])
			end

			return true
		end

		return false
	end

	function NewRaycastWeaponBase:fire(...)
		local ray_res = NewRaycastWeaponBase.super.fire(self, ...)
		local bullets_fired = self.parent_weapon and self.parent_weapon:base() and self.parent_weapon:base()._bullets_fired or self._bullets_fired

		if self._fire_mode == ids_burst and bullets_fired > 1 and not self:weapon_tweak_data().sounds.fire_single then
			self:_fire_sound()
		end

		return ray_res
	end