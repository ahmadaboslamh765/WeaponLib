function WeaponUnderbarrel:setup_data(setup_data, damage_multiplier, ammo_data, parent_base)
	self._alert_events = setup_data.alert_AI and {} or nil
	self._alert_fires = {}
	self._autoaim = setup_data.autoaim
	self._setup = setup_data
	self._parent_base = parent_base
end

function WeaponUnderbarrel:play_tweak_data_sound(event, alternative_event)
	local event = self:_get_sound_event(nil, event, alternative_event)

	if event then
		self:play_sound(event)
	end
end

function WeaponUnderbarrel:_get_sound_event(weapon, event, alternative_event)
	local sounds = self._tweak_data.sounds
	local event = sounds and (sounds[event] or sounds[alternative_event])

	return event
end

function WeaponUnderbarrel:get_name_id()
	return self.name_id
end

function WeaponUnderbarrelLauncher:play_tweak_data_sound(event, alternative_event)
	return WeaponUnderbarrel.play_tweak_data_sound(self, event, alternative_event)
end

function WeaponUnderbarrelLauncher:_get_sound_event(weapon, event, alternative_event)
	return WeaponUnderbarrel._get_sound_event(self, weapon, event, alternative_event)
end

function WeaponUnderbarrelLauncher:get_name_id()
	return self.name_id
end

function WeaponUnderbarrelRaycast:play_tweak_data_sound(event, alternative_event)
	return WeaponUnderbarrel.play_tweak_data_sound(self, event, alternative_event)
end

function WeaponUnderbarrelRaycast:_get_sound_event(weapon, event, alternative_event)
	return WeaponUnderbarrel._get_sound_event(self, weapon, event, alternative_event)
end

Hooks:PostHook(WeaponUnderbarrelShotgunRaycast, "init", "weaponlib_weaponunderbarrelshotgunraycast_init", function(self, unit)
	self._use_shotgun_reload = tweak_data.weapon[self._name_id].use_shotgun_reload or false
end)

WeaponUnderbarrelFlamethrower = WeaponUnderbarrelFlamethrower or class(WeaponUnderbarrelRaycast)

mixin(WeaponUnderbarrelFlamethrower, NewRaycastWeaponBase)
mixin(WeaponUnderbarrelFlamethrower, NewFlamethrowerBase)

function WeaponUnderbarrelFlamethrower:init(unit)
	self._blueprint = {}
	self._parts = {}

	self._last_fire_from_pos = Vector3(0,0,0)
	self._last_fire_direction = Vector3(0,0,1)

	WeaponUnderbarrel.init(self, unit)
	NewFlamethrowerBase.init(self, unit)
end

function WeaponUnderbarrelFlamethrower:play_tweak_data_sound(event, alternative_event)
	return WeaponUnderbarrel.play_tweak_data_sound(self, event, alternative_event)
end

function WeaponUnderbarrelFlamethrower:_get_sound_event(weapon, event, alternative_event)
	return WeaponUnderbarrel._get_sound_event(self, weapon, event, alternative_event)
end

function WeaponUnderbarrelFlamethrower:fire_mode()
	return WeaponUnderbarrel.fire_mode(self)
end

function WeaponUnderbarrelFlamethrower:ammo_base()
	return self._ammo
end

function WeaponUnderbarrelFlamethrower:_get_tweak_data_weapon_animation(anim)
	return WeaponUnderbarrel._get_tweak_data_weapon_animation(self, anim)
end

function WeaponUnderbarrelFlamethrower:can_toggle_firemode()
	return WeaponUnderbarrel.can_toggle_firemode(self)
end

function WeaponUnderbarrelFlamethrower:reload_prefix()
	return WeaponUnderbarrel.reload_prefix(self)
end

function WeaponUnderbarrelFlamethrower:is_single_shot()
	return WeaponUnderbarrel.is_single_shot(self)
end

function WeaponUnderbarrelFlamethrower:_fire_raycast(weapon_base, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
	self._last_fire_from_pos = from_pos
	self._last_fire_direction = direction

	return NewFlamethrowerBase._fire_raycast(self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
end

function WeaponUnderbarrelFlamethrower:_spawn_muzzle_effect()
	NewFlamethrowerBase._spawn_muzzle_effect(self, self._last_fire_from_pos, self._last_fire_direction)

	return nil
end
