function PlayerStandard:_check_action_deploy_underbarrel(t, input)
	if _G.IS_VR then
		if not input.btn_weapon_firemode_press and not self._toggle_underbarrel_wanted then
			return
		end
	elseif not input.btn_deploy_bipod and not self._toggle_underbarrel_wanted then
		return
	end

	if self._running and not self._equipped_unit:base():run_and_shoot_allowed() and not self._end_running_expire_t then
		self:_interupt_action_running(t)

		self._toggle_underbarrel_wanted = true

		return
	end

	local new_action = nil
	local action_forbidden = self:in_steelsight() or self:_is_throwing_projectile() or self:_is_meleeing() or self:is_equipping() or self:_changing_weapon() or self:shooting() or self:_is_reloading() or self:is_switching_stances() or self:_interacting() or self:running() and not self._equipped_unit:base():run_and_shoot_allowed()
	if not action_forbidden then
		self._toggle_underbarrel_wanted = false
		local weapon = self._equipped_unit:base()

		if weapon.record_fire_mode then
			weapon:record_fire_mode()
		end

		local underbarrel_state = weapon:underbarrel_toggle()

		if underbarrel_state ~= nil then
			local underbarrel_name_id = weapon:underbarrel_name_id()
			local underbarrel_tweak = tweak_data.weapon[underbarrel_name_id]
			new_action = true

			if weapon.reset_cached_gadget then
				weapon:reset_cached_gadget()
			end

			if weapon._update_stats_values then
				weapon:_update_stats_values(true)
			end

			local anim_name_id = nil
			local anim_redirect = nil
			local switch_delay = 1

			local weapon_tweak_data = weapon:weapon_tweak_data()

			if weapon_tweak_data.animations then
				if underbarrel_state then
					anim_name_id = weapon_tweak_data.animations.underbarrel_enter_name_id
				else
					anim_name_id = weapon_tweak_data.animations.underbarrel_exit_name_id
				end
			end

			if not anim_name_id then
				anim_name_id = underbarrel_tweak.weapon_hold and underbarrel_tweak.weapon_hold or weapon:weapon_tweak_data().weapon_hold or weapon.name_id
			end

			local function try_redirect(redirect)
				return self._ext_camera:play_redirect(Idstring(redirect), 1)
			end

			if underbarrel_state then
				anim_redirect = "underbarrel_enter_" .. anim_name_id
				switch_delay = underbarrel_tweak.timers.equip_underbarrel

				self:set_animation_state("underbarrel")
			else
				anim_redirect = "underbarrel_exit_" .. anim_name_id
				switch_delay = underbarrel_tweak.timers.unequip_underbarrel

				self:set_animation_state("standard")
			end

			if anim_redirect then
				self._ext_camera:play_redirect(Idstring(anim_redirect), 1)
			end

			self:set_animation_weapon_hold(nil)
			self:set_stance_switch_delay(switch_delay)
			self:_stance_entered()

			if alive(self._equipped_unit) then
				managers.hud:set_ammo_amount(self._equipped_unit:base():selection_index(), self._equipped_unit:base():ammo_info())
				managers.hud:set_teammate_weapon_firemode(HUDManager.PLAYER_PANEL, self._unit:inventory():equipped_selection(), self._equipped_unit:base():fire_mode())
			end

			if underbarrel_tweak.custom then
				if underbarrel_tweak.based_on then
					underbarrel_name_id = underbarrel_tweak.based_on
				else
					local is_npc = string.ends(underbarrel_name_id, "_npc")
					underbarrel_name_id = "contraband_m203" .. ( is_npc and "_npc" or "" )
				end
			end

			managers.network:session():send_to_peers_synched("sync_underbarrel_switch", self._equipped_unit:base():selection_index(), underbarrel_name_id, underbarrel_state)
		end
	end

	return new_action
end

function PlayerStandard:_start_action_reload_enter(t)
	local weapon = self._equipped_unit:base()

	if weapon and weapon:can_reload() then
		managers.player:send_message_now(Message.OnPlayerReload, nil, self._equipped_unit)
		self:_interupt_action_steelsight(t)

		if not self.RUN_AND_RELOAD then
			self:_interupt_action_running(t)
		end

		local should_do_empty_reload = weapon:should_do_empty_reload()
		local base_reload_enter_expire_t = weapon:reload_enter_expire_t(not should_do_empty_reload)

		if base_reload_enter_expire_t and base_reload_enter_expire_t > 0 then
			local speed_multiplier = weapon:reload_speed_multiplier()

			local reload_prefix = weapon:reload_prefix() or ""
			local reload_name_id = weapon:reload_name_id()

			local reload_redirect = Idstring(reload_prefix .. "reload_enter_" .. reload_name_id)

			self._ext_camera:play_redirect(reload_redirect, speed_multiplier)

			self._state_data.reload_enter_expire_t = t + base_reload_enter_expire_t / speed_multiplier

			weapon:tweak_data_anim_play("reload_enter", speed_multiplier)

			return
		end

		self:_start_action_reload(t)
	end
end

function PlayerStandard:_start_action_reload(t)
	local weapon = self._equipped_unit:base()

	if weapon and weapon:can_reload() then
		local should_do_empty_reload = weapon:should_do_empty_reload()

		weapon:tweak_data_anim_stop("fire")

		local speed_multiplier = weapon:reload_speed_multiplier()
		local empty_reload = should_do_empty_reload and 1 or 0

		if weapon:use_shotgun_reload() then
			empty_reload = weapon:get_ammo_max_per_clip() - weapon:get_ammo_remaining_in_clip()
		end

		local tweak_data = weapon:weapon_tweak_data()
		local reload_anim = "reload_not_empty"
		local reload_prefix = weapon:reload_prefix() or ""
		local reload_name_id = weapon:reload_name_id()
		local reload_default_expire_t = 2.2
		local reload_tweak = tweak_data.timers.reload_not_empty

		if should_do_empty_reload then
			reload_anim = "reload"
			reload_default_expire_t = 2.6
			reload_tweak = tweak_data.timers.reload_empty
		end

		local reload_ids = Idstring(string.format("%s%s_%s", reload_prefix, reload_anim, reload_name_id))
		local result = self._ext_camera:play_redirect(reload_ids, speed_multiplier)

		self._state_data.reload_expire_t = t + (reload_tweak or weapon:reload_expire_t(not should_do_empty_reload) or reload_default_expire_t) / speed_multiplier

		weapon:start_reload()

		if not weapon:tweak_data_anim_play(reload_anim, speed_multiplier) then
			weapon:tweak_data_anim_play("reload", speed_multiplier)
		end

		self._ext_network:send("reload_weapon", empty_reload, speed_multiplier)
	end
end
