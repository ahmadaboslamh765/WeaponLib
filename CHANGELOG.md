1.4.2:
* Fixed burst crashing when used with the akimbo weapon base.

1.4.1:
* Changed backward compatibility checks to use new tweak data.

1.4.0:
* New firemode selection logic to provide support for new burst-fire. Including new tweak data `CAN_TOGGLE_SPECIFIC_FIREMODE`!
* Deep clone the output of cached data to prevent accidental editing of cached outputs.
* Added support for new bullet object `offset` parameter.

1.3.3:
* Fixed Potential Ammo Object Crash

1.3.2:
* Removed some underbarrel switching animation fallback code because it was breaking the akimbo underbarrel shotguns.
* Fixed post heist accuracy calculation logic.
* Fixed scope overlay not updating for resolution correctly.
* Added a potential fix for certain crashes related to tweak data overriding.

1.3.1:
* Fixed a potential crash when viewing certain attachments.

1.3.0:
* Built my own firing function which supports way more than the shotgun and normal raycast ones used to, let me know if this doesn't always act as expected.
* Fixed infinitely reloading AI.
* Fixed fire rate overrides not being applied.

1.2.6:
* Actually fixed the Full Speed Swarm stuff now.
* Fixed a menu crash.

1.2.5:
* Any raycast weapon can now use `rays`. ( This was primarily a stat tracking fix. )
* Shotgun base code now correctly checks for an underbarrel when firing.
* Fixed some grammar in the weapon modification UI.
* Full Speed Swarm compatibility should finally be fixed.

1.2.4:
* Fixed some dumb Tdlq Full Speed Swarm code from infecting my code and breaking it.

1.2.3:
* Removed a useless function, fixing an incompatibility with Full Speed Swarm.

1.2.2:
* Fixed some problematic override code, which primarily caused crashes when interacting with AI crew weapons.

1.2.1:
* Fixed some weapons having 0 clip size.
* More sanity checks.
* Removed underbarrel type limit.

1.2.0:
* Fixed player names not showing above scope overlays.
* Added `scope_overlay_border_color` to control scope overlay border color.
* Added a few extra sanity checks around getting the equipped weapon unit.
* Fixed underbarrels not having firing sounds.
* Fixed More Weapon Stats incompatibility.
* Fixed crash when going into custody.
* Fixed overriden magazine size not appearing in all menus.

1.1.2:
* Gave scope overlays their own panel, instead of accidentally using the generic workspace one.

1.1.1:
* Temporarily removed the total ammo mod changes until I can figure out what causes the ammo issues.

1.1.0:
* Fixed hooks not loading because of a comment which SuperBLT couldn't handle.
* Added fractional support to Total Ammo Mod. Allowing for down to 1% changes by using `0.2` increments!

**1.0.0**:
* Full rewrite release!
* I'm actually happy enough with this release to take WeaponLib out of beta. The vast majority of pre-existing issues have been fixed in this full rewrite.
* This also includes a few new features, including `chamber_size` and `reload_num` for every weapon type.
* Also a whole bunch of improvements to bullet objects.
* Several fixes to underbarrels which Overkill have neglected, including full auto support, and shell by shell reloading support!

---

0.4.8:
* I think I fixed that flamethrower other client crash.

0.4.7:
* Hotfixed the Hotfix

0.4.6:
* Hotfix for 8th Anniversary Cash Launcher thing.

0.4.5:
* Fixed an older piece of code that conflicted with More Weapon Stats due to an old misunderstanding of how Overkill's class implementation works.

0.4.4:
* Updated some internal code to include Overkill's stinky way of changing underbarrel projectiles.
* Unhardcoded underbarrel entering and exiting, now it uses the `weapon_hold` of the underbarrel tweak data, `weapon_hold` of the main weapon, and finally just the `id`, in that order of priority.

0.4.3:
* A tweak to the previous hotfix.
* Actually remembered to change the internal version this time.

0.4.2:
* Potential table adding issue fix.

0.4.1:
* Fixed some cosmetic related stuff.

**0.4.0**:
* Look, I genuinely don't know what changed. MWS has been out of date for so long and I've started two rewrites off-site.
* Some shit is probably fixed.
* Some shit is probably still broken.
* Some shit is probably now broken.

---

0.3.1:
* Added an extra check when generating unique blueprint keys for caching. Occasionally a blueprint would have a nil value inside it causing default Lua functions to freak out.

**0.3.0**:
* Fixed weapon scope effects not properly handling the players custom colour grading choice.
* Fixed some incompatibility issues with some stuff.
* Added `Requires Attachment` module.
* Changed some internal code so that default weapon parts get re-added properly when they get removed due to a forbids.
* Added `Custom Attachment Points Legacy`, maintaining backwards compatibility with new code was getting too messy.

---

**0.2.0**: 
* 'Weapon Tweak Data Overrides' rewritten again, fallback code works now and should prevent the majority of crashes related to it.
* 'Custom Attachment Points' now works with parts that would normally parent to an existing attachment, e.g. silencers.
* Added 'Different Akimbos' module to expand the visual functionality of akimbo weapons.
* Removed excess logging.
* Selection indexes should use the defined function for it now instead of using the tweak data in order to expand mod support.
* Updated the internal hook order.
* Added 'Weapon Factory Manager Caching', caches data in order to improve performance when modding weapons.
* Fixed 'Weapon Tweak Data Overrides' using the wrong tables for add and multiply.
* Added backwards compatibility for: 'New magazine size for weapons', 'Attachment Animations', and 'Fire Rate Multiplier'.
* Added warning for out of date tweak data.

---

0.1.2: 
* Moved in some fixes from the development copy.
* Custom Attachment Points should work properly 90% of the time now.
* The weapon tweak data stuff should produce correct tables most of the time now.
* General small fixes.

0.1.1: 
* Fixed the replacement audio file naming.
* Added some extra code which should hopefully stop a crash with RestorationMod.

**0.1.0**: 
* Initial Beta Release!